<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome Icon CSS-->
        <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">

        <!-- BootStrap CSS -->
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
        <!-- Camera Slider CSS -->
        <link href="assets/css/camera.css" rel="stylesheet" type="text/css"/>

        <!-- HTML5 Boilerplate CSS -->
        <link rel="stylesheet" href="assets/css/normalize.css">
        <link rel="stylesheet" href="assets/css/main.css">

        <!-- Customize CSS -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/media.css?t=10"/>

        <!--HTML5shiv Js-->
        <script src="assets/js/modernizr-3.5.0.min.js"></script>
    </head>
    <body>
        <!--Camera Slide-->
         <div class="camera_wrap">
             <?php
				$rs = array_diff(scandir(realpath('media')), array('..', '.'));
				if (!empty($rs)):
                    foreach ($rs AS $filename):
			?>
            <div data-src="media/<?php echo $filename ?>">
                <img src="media/<?php echo $filename ?>">
            </div>
            <?php endforeach; endif;?>
        </div>   <!--------Camera Slide End-->

        <!-- Jquery -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <script>window.jQuery || document.write('<script src="assets/js/jquery-3.2.1.min.js"><\/script>')</script>
        <!--Camera JS with Required jQuery Easing Plugin-->
        <script src="assets/js/easing.min.js" type="text/javascript"></script>
        <script src="assets/js/camera.min.js" type="text/javascript"></script>
        <!-- Bootstrap Js -->
        <script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Custom JS --->
        <script src="assets/js/plugins.js?v=1"></script>
    </body>
</html>
